require 'match'
require 'simulator'

RSpec.describe Simulator do
  it 'simulates matches' do
    local_team = a_team
    away_team = a_team
    match = Match.of(local_team).against(away_team)

    result = Simulator.simulate(match)

    expect(result).not_to eq(nil)
  end

  def a_team
    @index ||= 0
    @index += 1

    "Team #{@index}"
  end
end
