require 'match'

RSpec.describe Match do
  it 'has a local team' do
    local = 'Valencia'

    match = Match.of(local)

    expect(match.local).to eq(local)
  end

  it 'has an away team' do
    away = 'Barcelona'

    match = Match.against(away)

    expect(match.away).to eq(away)
  end

  it 'prints both teams' do
    local = 'Valencia'
    away = 'Barcelona'

    match = Match.of(local).against(away)

    expect(match.to_s).to eq('Valencia vs Barcelona')
  end
end
