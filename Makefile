build:
	docker build -t total:latest .

shell: build
	docker run --rm -v $$(pwd):/opt/app -it total /bin/sh

test: build
	docker run --rm -it total
