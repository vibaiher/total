class Match
  class << self
    def of(team)
      Match.new.of(team)
    end

    def against(team)
      Match.new.against(team)
    end
  end

  attr_reader :local, :away

  def of(team)
    @local = team

    self
  end

  def against(team)
    @away = team

    self
  end

  def to_s
    "#{@local} vs #{@away}"
  end
end
