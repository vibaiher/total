FROM ruby:2.6.3-alpine3.10

ENV APP /opt/app
WORKDIR $APP

COPY Gemfile Gemfile.lock $APP/
RUN bundle install

COPY . .

CMD ["bundle", "exec", "rspec"]
